## ---- include = FALSE---------------------------------------------------------
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)

library(dplyr)
library(ggplot2)


## ----echo = FALSE-------------------------------------------------------------

RandomBreaks        <- vrpoptima:::RandomBreaks
CalculateTourLength <- vrpoptima:::CalculateTourLength
CalculateRange      <- vrpoptima:::CalculateRange
VehicleRouting      <- vrpoptima:::VehicleRouting
RoutesDataPrep      <- vrpoptima:::RoutesDataPrep
PlotToursCombined   <- vrpoptima:::PlotToursCombined
PlotToursIndividual <- vrpoptima:::PlotToursIndividual


## ----warning=FALSE, message=FALSE---------------------------------------------
library(vrpoptima)
data("visit_points_us")
data("vehicle_points_us")
patient_mat <- as.matrix(visit_points_us)
dist_mat    <- as.matrix(geodist::geodist(patient_mat, measure = 'haversine')/1000)


## ----warning = FALSE, message=FALSE-------------------------------------------
system.time(
solution <-  VehicleRouting(visit_points = patient_mat,
                           num_agents = nrow(vehicle_points_us),
                           agent_points = as.matrix(vehicle_points_us),
                           cost_type = 2,
                           max_tour_distance = 15000,
                           max_tour_visits = 50,
                           distance_metric = 'Geodesic',
                           distance_matrix = dist_mat,
                           min_tour = 2,
                           population_size = 96,
                           num_generations = 15000, 
                           distance_truncation = FALSE,
                           seed = 1234)

)



## ----warning=FALSE, message=FALSE---------------------------------------------
DT::datatable(t(solution$routes),
               rownames = TRUE,
               caption = 'Table 1. Optimal Routes for the 15 Vehicles',
               class = 'cell-border stripe',
               extensions=c("Buttons", 'Scroller'),
               options = list(dom = 'Bfrtip', 
                              columnDefs = list(list(className = 'dt-center', targets = 0:nrow(vehicle_points_us))),
                              scrollY = 380,
                              scroller = TRUE,
                              scrollX = TRUE), 
               escape = FALSE)


## ----warning=FALSE, message=FALSE, fig.align='c', fig.width=8, fig.height=6----
routes <- solution$routes
rownames(routes) <- 1:nrow(routes)
routes_list = RoutesDataPrep(routes = solution$routes, 
                             visit_points = as.matrix(visit_points_us), 
                             agent_points = as.matrix(vehicle_points_us))

PlotToursCombined(solution = solution,
                  routes_list = routes_list,
                  agent_locations =  vehicle_points_us,
                  orientation = 'vertical')


## ----warning=FALSE, message=FALSE, fig.align='c', fig.width=10, fig.height=10----
PlotToursIndividual(solution = solution, routes_list = routes_list)

## ----warning=FALSE, message=FALSE---------------------------------------------
data.frame(tour_length = solution$tour_lengths, 
           vehicle = 1:length(solution$tour_lengths))%>%
  dplyr::mutate_if(is.numeric, round, 1) %>% 
  DT::datatable(caption = 'Tour Lengths (km)')


